//
//  IndicatorView.swift
//  mySharp
//
//  Created by Bola Fayez on 2/18/18.
//  Copyright © 2018 Crossworkers. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwifterSwift

@IBDesignable
/// this Activity Indicator UIView class
class ActivityIndicatorView: UIView {
    
    // MARK: - Properties
    
    var activityIndicator: NVActivityIndicatorView!
    
    @IBInspectable var color: UIColor = Colors.primaryColor {
        didSet {
            activityIndicator.color = color
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupView()
    }
    
    // MARK: - Methods
    
    func setupView() {
        self.backgroundColor = UIColor.clear
        
        let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: self.frame.width, height: self.frame.height))
        let type = NVActivityIndicatorType.lineScale
        activityIndicator = NVActivityIndicatorView(frame: frame, type: type, color: color, padding: nil)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        
        // constrains for the indicator view
        let hConstraint = NSLayoutConstraint(item: activityIndicator as Any, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        let centerConstraint = NSLayoutConstraint(item: activityIndicator as Any, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0)

        addConstraint(hConstraint)
        addConstraint(centerConstraint)
        
    }
    
    /// this method makes the indicator start animating
    func startAnimating() {
        activityIndicator.startAnimating()
    }
    
    /// this method makes the indicator stop animating
    func stopAnimating() {
        activityIndicator.stopAnimating()
    }
    
    
}
