//
//  Colors.swift
//  iCare
//
//  Created by Bola Fayez on 8/17/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
}

struct Colors {
    
    public static let primaryColor = UIColor.init(r: 47, g: 84, b: 135)
    
}
