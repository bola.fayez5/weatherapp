//
//  CommenMethods.swift
//  
//
//  Created by Bola Fayez on 7/11/19.
//

import UIKit

class CommenMethods {

    static func AlertInfo (title: String, message: String, OkAction: String) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: OkAction)
        alert.show()
    }
    

}
