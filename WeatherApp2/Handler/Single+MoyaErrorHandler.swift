//
//  Single+MoyaErrorHandler.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import RxSwift
import Moya

/// Extending Single, adding subscribeWithDefaultErrorHandling method that has the default error handling mechanism.
extension PrimitiveSequenceType where Self.TraitType == RxSwift.SingleTrait {
    
    /**
     Subscribe on the Single. This subscription has the default error handling for Moya.
     - parameter onSuccess: Closure that accepts the element.
     - parameter onError: Closure that get called if error arises.
     - parameter viewModel: The ViewModel that will send the error message.
     - parameter errorMessageSize: Message size that shows the error (if found).
     - returns: Subscription object used to unsubscribe from the Single sequence.
     */
    func subscribeWithDefaultErrorHandling(onSuccess: @escaping ((Self.ElementType) -> Void), onError: ((Error) -> Void)? = nil, viewModel: BaseViewModel, errorMessageSize: MessageSize = .large) -> Disposable {
        return self.subscribe(onSuccess: { element in
            onSuccess(element)
        }, onError: { error in
            
            onError?(error)
            
            var errorMessage: String!
            do {
                if let errorResponse = error as? Moya.MoyaError, let errorModel = try errorResponse.response?.map(to: ErrorModel.self) {
                    // Log.error("Error Message: \(errorModel.errors[0].message)")
                    errorMessage = errorModel.message
                } else {
                    //Log.error("Response error: \(error.localizedDescription)")
                    errorMessage = error.localizedDescription
                }
            } catch let parseError {
                //Log.error("Parse error: \(parseError.localizedDescription)")
                errorMessage = parseError.localizedDescription
            }
            
            switch errorMessageSize {
            case .large:
                CommenMethods.AlertInfo(title: "", message: errorMessage, OkAction: "OK")
            case .lite:
                CommenMethods.AlertInfo(title: "", message: errorMessage, OkAction: "OK")
            }
        })
    }
}

