//
//  NavigationUtil.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import SwinjectStoryboard

/// A utility struct that contains many view controllers initialization methods.
struct NavigationUtil {
    
    /// Initialize the Authentication view controller.
    static func initAuthVC() -> UIViewController {
        let storyboard = AppDelegate.mainAssembler.resolver.resolve(SwinjectStoryboard.self, name: "Main")!
        return storyboard.instantiateInitialViewController()!
    }
    
}
