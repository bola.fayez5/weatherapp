//
//  MainViewController.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/15/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import CoreLocation
import RxCocoa
import RxSwift
import GooglePlacesSearchController

class MainViewController: BaseViewController<MainViewModel> {

    // MARK: - Outlests and Values
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableViewCities: UITableView!
    @IBOutlet weak var indecatorView: ActivityIndicatorView!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var locationManager: CLLocationManager!    
    
    lazy var placesSearchController: GooglePlacesSearchController = {
        let controller = GooglePlacesSearchController(delegate: self,
                                                      apiKey: GoogleMapsAPIServerKey,
                                                      placeType: .address)
        return controller
    }()
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBinding()
        setupViews()
        regsiterCellTableView()
    }

    // MARK: - setupViews
    func setupViews() {
        
        locationManager = CLLocationManager()

        if (CLLocationManager.locationServicesEnabled()) && RealmCitisWeatherModel.getCountArraySavedWeather(realm: self.viewModel.repoCities.realm!) < 1{
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            locationManager.delegate = nil
            self.viewModel.getCityWeather(city: "")
        }
        
    }
    
    // MARK: - regsiterCellTableView
    func regsiterCellTableView () {
        tableViewCities.register(UINib(nibName: "WeatherDayTableViewCell", bundle: nil), forCellReuseIdentifier: "dayWeatherCell")
    }
    
    // MARK: - SetupBinding
    func setupBinding() {
                
        searchBtn.rx.tap.bind {
            self.present(self.placesSearchController, animated: true, completion: nil)
        }.disposed(by: disposeBag)
        
        viewModel.cityWeatherResponse.asSignal().emit(onNext: { response in
            self.tableViewCities.reloadData()
        }).disposed(by: disposeBag)
        
        deleteBtn.rx.tap.bind {
            self.viewModel.weatherDaysArray = []
            self.viewModel.repoCities.deleteFromRealm()
            self.tableViewCities.reloadData()
        }.disposed(by: disposeBag)
        
//        viewModel.cityWeatherResponse.asDriver().drive(onNext: { _ in
//            self.tableViewCities.reloadData()
//        }).disposed(by: disposeBag)
        
        // traking indicators
        viewModel.startIndicator.asSignal().emit(onNext: { indicatorIsAnimating in
            if indicatorIsAnimating {
                self.indecatorView?.startAnimating()
                self.tableViewCities.reloadData()
            } else {
                self.indecatorView?.stopAnimating()
                self.tableViewCities.reloadData()
            }
        }).disposed(by: disposeBag)
        
    }


}

// MARK: - Extention for TableView Deleagtes
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.viewModel.weatherDaysArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableViewCities.dequeueReusableCell(withIdentifier: "dayWeatherCell", for: indexPath) as? WeatherDayTableViewCell
        cell?.city.text = self.viewModel.weatherDaysArray[indexPath.row].name
        cell?.degree.text = String(Double(self.viewModel.weatherDaysArray[indexPath.row].temp - 273.15).rounded()) + String(format:"23%@", "\u{00B0}")
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let city = self.viewModel.weatherDaysArray[indexPath.row].name
        let daysWeatherViewController = AppDelegate.mainAssembler.resolver.resolve(DaysWeatherViewController.self, argument: city)!
        self.present(daysWeatherViewController, animated: true, completion: nil)
        
    }
    
    
}

// MARK: - Extention for GooglePlacesAutocompleteViewControllerDelegate
extension MainViewController: GooglePlacesAutocompleteViewControllerDelegate {
    
    func viewController(didAutocompleteWith place: PlaceDetails) {
        self.viewModel.getCityWeather(city: place.subAdministrativeArea ?? "")
        self.tableViewCities.reloadData()
        placesSearchController.isActive = false
    }
    
}

// MARK: - Extention for CLLocationManagerDelegate
extension MainViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last! as CLLocation

        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0 {
                let placemark = placemarks![0]
                self.viewModel.getCityWeather(city: placemark.locality ?? "")
                self.tableViewCities.reloadData()

            }

        }
        
        locationManager.delegate = nil

    }

}
