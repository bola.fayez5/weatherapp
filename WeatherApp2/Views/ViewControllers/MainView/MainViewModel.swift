//
//  MainViewModel.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/15/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Moya
import Moya_ModelMapper
import RxCocoa
import RxSwift
import UIKit
import MapKit
import SwiftLocation

class MainViewModel: BaseViewModel {

    // MARK: - Properties
    //let api: MoyaProvider<GetWeatherAPI>
    var currentlocationCoordinate: BehaviorRelay = BehaviorRelay <CLLocationCoordinate2D>(value: CLLocationCoordinate2D())
    var cityWeatherResponse = PublishRelay<WeatherModel>()
    var weatherDaysArray = [WeatherModel]()
    let startIndicator = PublishRelay<Bool> ()
    let repoCities: CitiesWeatherRepository
    
    // MARK: - Initializer
    init(repo: CitiesWeatherRepository) {
        self.repoCities = repo
    }
    
    // MARK: - getCityWeather
    func getCityWeather(city: String) {
        self.weatherDaysArray = []
        startIndicator.accept(true)
        repoCities.getCities(city: city).subscribe(onNext: { weather in
            self.cityWeatherResponse.accept(weather)
            self.weatherDaysArray.append(weather)
            self.startIndicator.accept(false)
        }).disposed(by: disposeBag)
    }
    
//    //MARK: - this function make api request for Get Weather by City name
//    func getCityWeather(city: String) {
//        startIndicator.accept(true)
//        api.rx.request(.getCityWeather(city, APPKEY))
//            .map(to: WeatherModel.self)
//            .subscribeWithDefaultErrorHandling(onSuccess: { response in
//                if self.weatherDaysArray.count <= 4 {
//                    self.weatherDaysArray.append(response)
//                    self.cityWeatherResponse.accept(response)
//                    self.startIndicator.accept(false)
//                } else {
//                    CommenMethods.AlertInfo(title: "", message: "Sorry max 5 days only.", OkAction: "OK")
//                }
//            }, onError: { error in
//                self.startIndicator.accept(false)
//                self.messageEvent.accept((error.localizedDescription, .error))
//            }, viewModel: self, errorMessageSize: .large)
//            .disposed(by: disposeBag)
//    }
    
    // MARK: - getCurrentLocation
    func getCurrentLocation() {
        Locator.currentPosition(accuracy: .neighborhood, onSuccess: { loc in
                self.currentlocationCoordinate.accept(loc.coordinate)
        }, onFail: {err, _ in
            self.messageEvent.accept(( err.localizedDescription, .error))
        })
    }
    
}
