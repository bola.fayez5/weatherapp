//
//  BaseViewModel.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/15/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftMessages

class BaseViewModel {
    
    // MARK: - Properties
    
    let disposeBag = DisposeBag()
    
    /// Used to show a large `SwiftMessages` message, specifiying a message body and a theme.
    let messageEvent = PublishRelay<(String, Theme)>()
    /// Used to show a single line `SwiftMessages` message, specifiying a message body and a theme.
    let liteMessageEvent = PublishRelay<(String, Theme)>()
    
}

/// Enum that have two cases .large and .lite for determining SwiftyMessages message size.
enum MessageSize {
    case large
    case lite
}
