//
//  DaysWeatherViewController.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DaysWeatherViewController: BaseViewController<DaysWeatherViewModel> {

    // MARK: - Outlests and Values
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableViewDayes: UITableView!
    @IBOutlet weak var indecatorView: ActivityIndicatorView!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBinding()
        regsiterCellTableView()
    }

    // MARK: - regsiterCellTableView
    func regsiterCellTableView () {
        tableViewDayes.register(UINib(nibName: "WeatherDayTableViewCell", bundle: nil), forCellReuseIdentifier: "dayWeatherCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    // MARK: - SetupBinding
    func setupBinding() {
        
        self.viewModel.getCityWeatherDays()

        viewModel.cityWeatherDayesResponse.asSignal().emit(onNext: { response in
            self.tableViewDayes.reloadData()
        }).disposed(by: disposeBag)
        
        // traking indicators
        viewModel.startIndicator.asSignal().emit(onNext: { indicatorIsAnimating in
            if indicatorIsAnimating {
                self.indecatorView?.startAnimating()
            } else {
                self.indecatorView?.stopAnimating()
            }
        }).disposed(by: disposeBag)
        
    }

}

// MARK: - Extention for TableView Deleagtes
extension DaysWeatherViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.arrayDayesWeather?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableViewDayes.dequeueReusableCell(withIdentifier: "dayWeatherCell", for: indexPath) as? WeatherDayTableViewCell
        cell?.city.text = "Day" + " \(indexPath.row + 1)"
        cell?.degree.text = String(Double((self.viewModel.arrayDayesWeather?.list[indexPath.row].temp)! - 273.15).rounded()) + String(format:"23%@", "\u{00B0}")
        return cell!
    }
    
}
