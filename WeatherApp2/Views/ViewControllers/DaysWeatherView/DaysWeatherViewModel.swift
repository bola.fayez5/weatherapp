//
//  DaysWeatherViewModel.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Moya
import Moya_ModelMapper
import RxCocoa
import RxSwift
import UIKit

class DaysWeatherViewModel: BaseViewModel {

    // MARK: - Properties
    let api: MoyaProvider<GetWeatherAPI>
    let city: String
    let startIndicator = PublishRelay<Bool> ()
    //var cityWeatherDayesResponse = BehaviorRelay<[WeatherDaysModel]>(value: [])
    var cityWeatherDayesResponse = PublishRelay<WeatherDaysModel>()
    var arrayDayesWeather: WeatherDaysModel?
    
    // MARK: - Initializer
    init(api: MoyaProvider<GetWeatherAPI>, city: String) {
        self.api = api
        self.city = city
    }
    
    // MARK: - getDaysWeatherByCity    
    func getCityWeatherDays() {
        self.startIndicator.accept(true)
        api.rx.request(.getWeatherDaysBtCity(self.city, APPKEY))
            .map(to: WeatherDaysModel.self)
            .subscribeWithDefaultErrorHandling(onSuccess: { response in
                self.arrayDayesWeather = response
                self.startIndicator.accept(false)
                self.cityWeatherDayesResponse.accept(response)
            }, onError: { error in
                self.messageEvent.accept((error.localizedDescription, .error))
                self.startIndicator.accept(false)
            }, viewModel: self, errorMessageSize: .large)
            .disposed(by: disposeBag)
    }
    
}
