//
//  ApiAssembly.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject
import Moya

// Swinject assembly for Moya APIs
class ApiAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(MoyaProvider<GetWeatherAPI>.self) { r in
            var plugins: [PluginType] = []
            if isDebug {
                plugins.append(NetworkLoggerPlugin(verbose: true, cURL: false, responseDataFormatter: MoyaJSONResponseDataFormatter))
            }
            return MoyaProvider<GetWeatherAPI>(plugins: plugins)
        }

    }
    
}
