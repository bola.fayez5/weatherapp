//
//  ViewModelsAssembly.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject
import Moya

/// Swinject assemblies for ViewModels
class ViewModelsAssembly: Assembly {
    
    func assemble(container: Container) {
        
//        container.register(MainViewModel.self) { r in
//            return MainViewModel(api: r.resolve(MoyaProvider<GetWeatherAPI>.self)!)
//        }
//
        container.register(MainViewModel.self) { r in
            return MainViewModel(repo: r.resolve(CitiesWeatherRepository.self)!)
        }
        
        container.register(DaysWeatherViewModel.self) { ( r, city: String) in
           return DaysWeatherViewModel(api: r.resolve(MoyaProvider<GetWeatherAPI>.self)!, city: city)
        }
        
    }
}
