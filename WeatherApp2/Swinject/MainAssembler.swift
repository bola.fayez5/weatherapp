//
//  MainAssembler.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Foundation
import Swinject
import SwinjectStoryboard

class MainAssembler {
    
    private let assembler = Assembler(container: SwinjectStoryboard.defaultContainer)
    
    var resolver: Resolver {
        return assembler.resolver
    }
    
    // init Assembler ...
    init() {
        
        assembler.apply(assembly: ApiAssembly()) // applying Api assembly
        
        assembler.apply(assembly: SwinjectStoryboardAssembly())
        
        assembler.apply(assembly: ViewControllersAssembly())
        
        assembler.apply(assembly: RepositoriesAssembly())
        
        assembler.apply(assembly: ViewModelsAssembly())
        
        assembler.apply(assembly: RealmAssembler())
        
    }
    
}
