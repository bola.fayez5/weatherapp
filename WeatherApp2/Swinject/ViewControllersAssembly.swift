//
//  ViewControllersAssembly.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject

/// Swinject assembly for ViewControllers
class ViewControllersAssembly: Assembly {
    
    func assemble(container: Container) {
    
        
        container.register(MainViewController.self) { _ in
            MainViewController(nibName: "MainViewController", bundle: nil)
            }.initCompleted { r, vc in
                vc.viewModel = r.resolve(MainViewModel.self)
        }

        
        container.register(DaysWeatherViewController.self) { (r, city: String) in
            let vc = DaysWeatherViewController(nibName: "DaysWeatherViewController", bundle: nil)
            vc.viewModel = r.resolve(DaysWeatherViewModel.self, argument: city)
            return vc
        }
        
    }
    
}
