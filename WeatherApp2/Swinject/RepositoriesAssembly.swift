//
//  RepositoriesAssembly.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject
import Moya
import RealmSwift

/// Swinject assemblis for repositores
class RepositoriesAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(CitiesWeatherRepository.self) { r in
            return CitiesWeatherRepository(api: r.resolve( MoyaProvider<GetWeatherAPI>.self )!, realm: r.resolve(Realm.self)!)
        }
        
    }
    
}
