//
//  SwinjectStoryboardAssembly.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

/// Swinject assembly for SwinjectStoryboard storyboard
class SwinjectStoryboardAssembly: Assembly {
    
    
    func assemble(container: Container) {
        
        container.register(SwinjectStoryboard.self, name:  "Main") { _ in
            return SwinjectStoryboard.create(name: "Main", bundle: nil)
        }
        
    }
    
    
}

