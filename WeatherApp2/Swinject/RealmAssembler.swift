//
//  RealmAssembler.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import Swinject
import RealmSwift

/// Swinject assembly for Realm
class RealmAssembler: Assembly {
    
    func assemble(container: Container) {
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        container.register(Realm.self) { _ in
            
            let config = Realm.Configuration(
                
                // Version of database ...
                schemaVersion: 1,
                
                migrationBlock: { _, oldSchemaVersion in
                    if oldSchemaVersion < 1 {
                        
                        // TODO :- Updates
                        
                    }
            })
            
            Realm.Configuration.defaultConfiguration = config
            
            return (try? Realm())!
            }.inObjectScope(.container)
    }
    
}

