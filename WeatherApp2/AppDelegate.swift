//
//  AppDelegate.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/15/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import UIKit
import RxSwift
import SwinjectStoryboard

let disposebage = DisposeBag()
let GoogleMapsAPIServerKey = "AIzaSyAuyCoT8muEyDeujUfHvgM8LV3dTjO1tZk"
let APPKEY = "3ed8bf18eeee883f9b9675a8c2a7b0d3"

/// Checks if it is debug or another (release) build. https://stackoverflow.com/a/47443568/6516499
let isDebug: Bool = {
    var isDebug = false
    // function with a side effect and Bool return value that we can pass into assert()
    func set(debug: Bool) -> Bool {
        isDebug = debug
        return isDebug
    }
    // assert:
    // "Condition is only evaluated in playgrounds and -Onone builds."
    // so isDebug is never changed to true in Release builds
    assert(set(debug: true))
    return isDebug
}()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let mainAssembler = MainAssembler() // Singleton in the application

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        setupWindow()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    /// Initialize root view controller with SwinjectStoryboard, this is necessary for injecting UIViewControllers.
    func setupWindow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window
        window.backgroundColor = UIColor.black
        window.rootViewController = AppDelegate.mainAssembler.resolver.resolve(MainViewController.self)

    }

}

