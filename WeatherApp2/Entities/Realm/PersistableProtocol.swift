//
//  PersistableProtocol.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import RealmSwift

public protocol Persistable {
    
    associatedtype ManagedObject: RealmSwift.Object
    
    init(managedObject: ManagedObject)
    
    func managedObject() -> ManagedObject
}

