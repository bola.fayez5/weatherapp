//
//  RealmCitisWeatherModel.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import RealmSwift

class RealmCitisWeatherModel: Object {

    @objc dynamic var name: String = ""
    @objc dynamic var temp: Double = 0

    func save(realm: Realm) {
        try? realm.write {
            realm.add(self)
        }
    }
    
    static func getAllCities(realm: Realm) -> Results<RealmCitisWeatherModel> {
        let result = realm.objects(RealmCitisWeatherModel.self)
        return result
    }
    
    static func getCountArraySavedWeather (realm: Realm) -> Int {
        let result = realm.objects(RealmCitisWeatherModel.self)
        return result.count
    }
    
    static func deletAll(realm: Realm) {
        try? realm.write {
            realm.delete(getAllCities(realm: realm))
        }
    }

}
