//
//  ErrorModel.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/15/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Mapper

struct ErrorModel: Mappable {
    
    let cod: String
    let message: String
    
    init(map: Mapper) throws {
        try cod = map.from("cod")
        try message = map.from("message")
    }
    
}
