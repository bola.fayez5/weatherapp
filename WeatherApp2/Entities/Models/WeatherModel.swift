//
//  WeatherModel.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Mapper

struct WeatherModel: Mappable {
    
    var name: String
    var temp: Double

    init(map: Mapper) throws {
        try name = map.from("name")
        try temp = map.from("main.temp")
    }

}

extension WeatherModel: Persistable {
    
    public init(managedObject: RealmCitisWeatherModel) {
        name = managedObject.name
        temp = managedObject.temp
    }
    
    public func managedObject() -> RealmCitisWeatherModel {
        
        let cityRequest = RealmCitisWeatherModel()
        cityRequest.name = name 
        cityRequest.temp = temp 
        return cityRequest
    }
}
