//
//  WeatherDaysModel.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/17/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Mapper

struct WeatherDaysModel: Mappable {
    
    var list: [List]
        
    init(map: Mapper) throws {
        try list = map.from("list")
    }

}

struct List: Mappable {
    
    var temp: Double
    
    init(map: Mapper) throws {
        try temp = map.from("temp.day")
    }
    
}
