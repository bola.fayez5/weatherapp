//
//  CitiesWeatherRepository.swift
//  WeatherApp2
//
//  Created by Bola Fayez on 11/16/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Moya
import RealmSwift
import RxSwift
import RxRealm

class CitiesWeatherRepository {

    // MARK: - properties
    var api: MoyaProvider<GetWeatherAPI>
    var realm: Realm?
    
    // MARK: - intializers
    init(api: MoyaProvider<GetWeatherAPI>, realm: Realm) {
        self.api = api
        self.realm = realm
    }
    
    // MARK: - requestApi
    func requestApi(city: String) -> Observable<WeatherModel> {
        
        return api.rx.request(.getCityWeather(city, APPKEY))
            .map(to: WeatherModel.self)
            .do(onSuccess: {cityRespons in
                
                let count = RealmCitisWeatherModel.getCountArraySavedWeather(realm: self.realm!)
                if count <= 4 {
                    //RealmCitisWeatherModel.deletAll(realm: (self.realm)!)
                    self.storeToRealm(models: cityRespons)
                } else {
                    CommenMethods.AlertInfo(title: "", message: "Sorry max 5 days only.", OkAction: "OK")
                }
                
            })
        .asObservable()
    }
    
    // MARK: - fetchFromRealm
    func getFromRealmCitiesModel() -> Observable<WeatherModel> {
        let cityResult = realm!.objects(RealmCitisWeatherModel.self)
        
        return Observable.from(cityResult).map({ city in
            WeatherModel(managedObject: city)
        })
        
//        return Observable.array(from: cityResult).map({ cityObjects in
//            cityObjects.map({ object in
//                WeatherModel(managedObject: object)
//            }).first!
//        })
    }
    
    // MARK: - getCities from both
    func getCities(city: String) -> Observable<WeatherModel> {
        
        let apiRequest = requestApi(city: city).share()
        
        return Observable.merge(apiRequest, getFromRealmCitiesModel().takeUntil(apiRequest).takeWhile { (element) -> Bool in
            return element.name != nil
        })
        
        //return Observable.merge(getFromRealmCitiesModel(), apiRequest).takeUntil(getFromRealmCitiesModel())
    }
    
    // MARK: - storeToRealm
    func storeToRealm(models: WeatherModel) {
        models.managedObject().save(realm: realm!)
    }

    // MARK: - deleteFromRealm
    func deleteFromRealm () {
        RealmCitisWeatherModel.deletAll(realm: self.realm!)
    }
    
}
