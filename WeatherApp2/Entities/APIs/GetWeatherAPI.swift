//
//  GetWeatherAPI.swift
//  WeatherApp
//
//  Created by Bola Fayez on 11/14/19.
//  Copyright © 2019 Bola Fayez. All rights reserved.
//

import Moya

enum GetWeatherAPI {
    case getCityWeather(String, String)
    case getWeatherDaysBtCity(String, String)
}

extension GetWeatherAPI: TargetType {
    
    var baseURL: URL {
        
        switch self {
        case .getCityWeather:
            return URL(string: "https://api.openweathermap.org/data/2.5/weather/")!
        case .getWeatherDaysBtCity:
            return URL(string: "https://samples.openweathermap.org/data/2.5/forecast/daily")!
        }
                
    }
    
    var path: String {
        return ""
    }
    
    var method: Method {
        switch self {
        case .getCityWeather, .getWeatherDaysBtCity:
            return .get
        }
        
    }
    
    var task: Task {
        switch self {
            
        case .getCityWeather(let city, let APPID), .getWeatherDaysBtCity(let city, let APPID):
            return .requestParameters(parameters: ["q": city, "APPID": APPID], encoding: URLEncoding.default)
            
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    var validate: Bool { // Treat non 2XX response codes as error (onError)
        return true
    }
    
}

